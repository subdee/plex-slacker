# Plex Slacker

A small background service that listens to a Plex Media Server webhook and updates your Slack status.

## Requirements

- Plex Media Server
- Slack channel with permissions to use the API with an OAuth token
- Docker/Docker Compose or a small server with php cli

## Usage

### With Docker

Using the included Dockerfile, you can build an image with your settings and run it in a container. Then point your Plex webhook there

### With a web server

Make the index.php available on some web server and domain and point your Plex webhook there