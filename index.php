<?php

use GuzzleHttp\Client as ClientAlias;

require_once __DIR__ . '/vendor/autoload.php';

if (false === isset($_POST['payload'])) {
    fail();
}

$payload = json_decode($_POST['payload']);

if (JSON_ERROR_NONE !== json_last_error()) {
    fail();
}

if ($payload->Metadata->librarySectionType !== 'artist') {
    skip();
}

if ($payload->event !== 'media.play') {
    skip();
}

$text = $payload->Metadata->grandparentTitle . ' - ' . $payload->Metadata->title;

$client = new ClientAlias([
    'base_uri' => 'https://slack.com/api/'
]);

$response = $client->post('users.profile.set', [
    'json' => [
        'profile' => [
            'status_text' => $text,
            'status_emoji' => ':notes:',
            'status_expiration' => 0
        ]
    ],
    'headers' => [
        'Authorization' => 'Bearer <your-oauth-token-here>'
    ]
]);

if ($response->getStatusCode() === 200) {
    succeed();
} else {
    fail();
}

function succeed()
{
    echo 'OK COMPUTER';
    echo "\n";
    exit();
}

function fail()
{
    echo 'OK NOTOK';
    echo "\n";
    exit();
}

function skip()
{
    echo 'OK SKIP';
    echo "\n";
    exit();
}
